package com.nextclick.deliveryboy.utils;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.nextclick.deliveryboy.R;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Map.DirectionsJSONParser;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Map.MapParser;
import com.nextclick.deliveryboy.dashboard.activitys.ui.Map.MapRouteNavigationHelper;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DirectionUtil {

//    final MapParser listener;
    static  Context mContext;

    public static void getDirectionData(Context mContext, LatLng mOrigin, LatLng mDestination, DirectionListener listener) {
        DirectionUtil.mContext = mContext;
        String url = getDirectionsUrl(mOrigin, mDestination);
        DirectionDownloadTask downloadTask = new DirectionDownloadTask(listener);
        downloadTask.execute(url);
    }

    private static String getDirectionsUrl(LatLng origin, LatLng dest){

        //https://developers.google.com/maps/documentation/directions/get-directions

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Key
        String key = "key=" + mContext.getString(R.string.google_maps_key);

        //Travel modes -driving(default),walking ,bicycling  ,transit
        String mode = "&mode=transit";//"";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+key+mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }


}
