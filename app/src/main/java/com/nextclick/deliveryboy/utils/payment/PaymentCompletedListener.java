package com.nextclick.deliveryboy.utils.payment;

public interface PaymentCompletedListener {
    void onPaymentCompleted(Integer paymentStatus, String paymentID, String error);
}
