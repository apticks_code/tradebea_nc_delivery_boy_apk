package com.nextclick.deliveryboy.Config;

public interface Config {

    //Main live

     // public String BASE_URL = "https://nextclick.in/app/";


    // Test
   //  public String BASE_URL = "http://test.nextclick.in/";
    //New test
    public String BASE_URL="http://newtest.nextclick.in/";

    // Dev

   //  public String BASE_URL = "http://dev.nextclick.in/";

    // public String BASE_URL="http://13.233.215.21/";//aws


   //mew Test
   //  public String BASE_URL = "http://newtest.nextclick.in/";

    public String LOGIN = BASE_URL + "/auth/api/auth/login";
    public String REGISTER_USER = BASE_URL + "auth/api/auth/register";
    public String USER_PROFILE = BASE_URL + "user/profile/me?intent=delivery_partner";
    public String FCM = BASE_URL + "general/api/fcm_notify/grant_fcm_permission";
    public String REGISTER = BASE_URL +"user/profile/manage";// "auth/api/auth/register/delivery_partner";
    public String PROFILE_READ = BASE_URL + "user/master/profile/r";
    public String PROFILE_UPDATE = BASE_URL + "user/master/profile/u";
   // public String UPDATE_PROFILE_DETAILS = BASE_URL + "vendor/api/vendor/profile/profile";

    public String FORGOT_PASSWORD = BASE_URL + "auth/api/auth/forgot_password";


    public String MANAGE_LOGIN_SESSION = BASE_URL + "delivery/api/delivery/manage_login_session";
    public String UPDATE_CURRENT_LOCATION = BASE_URL + "delivery/api/delivery/current_location/set";
    public String GET_NEAREST_VENDORS = BASE_URL + "delivery/api/delivery/near_by_vendors";
    public String GET_NOTIFICATIONS = BASE_URL + "delivery/api/delivery/notifications/r";
    public String SET_NOTIFICATION_ACCEPT = BASE_URL + "delivery/api/delivery/notifications/accept";
    public String SET_NOTIFICATION_ACCEPT_NEW = BASE_URL + "delivery/api/delivery/accept";

    public String GET_ORDERS_LIST = BASE_URL + "delivery/api/orders/delivery_orders/r";
    public String CHANGE_DELIVERY_ORDER_STATUS = BASE_URL + "delivery/api/orders/delivery_orders/change_status";
    public String INVOKE_DELIVERY_ORDER = BASE_URL + "delivery/api/orders/delivery_orders/deliver";
    public String ORDERDETAILS = BASE_URL + "vendor/api/ecom/ecom_orders/order_details";
    public String CHANGE_PICKED_ORDER = BASE_URL + "delivery/api/orders/delivery_orders/picked";
    public String IMAGE_CONFIRMATION_PICKUP = BASE_URL + "delivery/api/orders/delivery_images/pickup";
    public String IMAGE_CONFIRMATION_DELIVERY = BASE_URL + "delivery/api/orders/delivery_images/delivery";

    public String WALLETHISTORY = BASE_URL + "payment/api/payment/wallet";
    public String GET_DASHBOARD_DETAILS = BASE_URL + "delivery/api/delivery/dashboard";

    //Payment Modes
    public String PAYMENTMODES=BASE_URL+"user/ecom/payment_methods";
    public String PAYMENTSTATUS=BASE_URL+"payment/api/payment/delivery_boy_wallet_topup";


    public String SUPPORT_QUERIES_LIST=BASE_URL+"general/api/support/support_queries/r";

    public String SUPPORT_QUERIES_TYPES=BASE_URL+"user/ecom/request_type/r";
    public String SUBMIT_FEEDBACK=BASE_URL+"general/api/support/support_queries/c";
    public String SUPPORTUPDATE = BASE_URL +"general/api/support/support_queries/u";


    public String SEND_OTP = BASE_URL + "auth/api/auth/otp";//AUthor-sunil//"auth/api/auth/otp_gen";
    public String VERIFY_OTP = BASE_URL +"auth/api/auth/validate_otp";//AUthor-sunil// "auth/api/auth/verify_otp";

    //terms and conditions
    public String GET_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/termsconditions";
    public String ACCEPT_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/accept_tc";
    public String VALIDATE_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/validate_user";

    public static final String URL_States = BASE_URL + "/general/api/master/states/";
    public static final String VEHICLE_TYPES = BASE_URL + "/general/api/master/vehicletype";

    public static final String REJECT_ORDERS=BASE_URL+"delivery/api/orders/delivery_orders/reject";
   // public static final String REJECT_ORDERS="http://192.168.29.65/nextclick/delivery/api/orders/delivery_orders/reject";
    public static final String RESONS_REJECTED=BASE_URL+"delivery/api/orders/delivery_rejected_reasons";
    public static final String RETURN_TO_VENDOR=BASE_URL+"delivery/api/return_order/delivery_boy_return/reched";


    public String PAYMENT_LINK = BASE_URL + "payment/api/payment/create_payment_link";//post


    //retun
    public String RETURN_REACHED = BASE_URL + "delivery/api/return_order/delivery_boy_return/reched";
    public String RETURN_ORDER = BASE_URL + "delivery/api/return_order/delivery_boy_return/retured_the_order";

}
